<?php

namespace App\Repository;

use App\Entity\BodyProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BodyProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method BodyProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method BodyProduct[]    findAll()
 * @method BodyProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BodyProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BodyProduct::class);
    }

    // /**
    //  * @return BodyProduct[] Returns an array of BodyProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BodyProduct
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
