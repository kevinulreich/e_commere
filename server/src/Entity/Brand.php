<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 */
class Brand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $joinedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailRepair;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BodyProduct", mappedBy="brand")
     */
    private $bodyProduct;

    public function __construct()
    {
        $this->bodyProduct = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoinedAt(): ?\DateTimeInterface
    {
        return $this->joinedAt;
    }

    public function setJoinedAt(\DateTimeInterface $joinedAt): self
    {
        $this->joinedAt = $joinedAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmailRepair(): ?string
    {
        return $this->emailRepair;
    }

    public function setEmailRepair(?string $emailRepair): self
    {
        $this->emailRepair = $emailRepair;

        return $this;
    }

    /**
     * @return Collection|BodyProduct[]
     */
    public function getBodyProduct(): Collection
    {
        return $this->bodyProduct;
    }

    public function addBodyProduct(BodyProduct $bodyProduct): self
    {
        if (!$this->bodyProduct->contains($bodyProduct)) {
            $this->bodyProduct[] = $bodyProduct;
            $bodyProduct->setBrand($this);
        }

        return $this;
    }

    public function removeBodyProduct(BodyProduct $bodyProduct): self
    {
        if ($this->bodyProduct->contains($bodyProduct)) {
            $this->bodyProduct->removeElement($bodyProduct);
            // set the owning side to null (unless already changed)
            if ($bodyProduct->getBrand() === $this) {
                $bodyProduct->setBrand(null);
            }
        }

        return $this;
    }
}
