<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\SubCategoryRepository")
 */
class SubCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="subCategories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BodyProduct", mappedBy="subCategory")
     */
    private $bodyProducts;

    public function __construct()
    {
        $this->bodyProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|BodyProduct[]
     */
    public function getBodyProducts(): Collection
    {
        return $this->bodyProducts;
    }

    public function addBodyProduct(BodyProduct $bodyProduct): self
    {
        if (!$this->bodyProducts->contains($bodyProduct)) {
            $this->bodyProducts[] = $bodyProduct;
            $bodyProduct->setSubCategory($this);
        }

        return $this;
    }

    public function removeBodyProduct(BodyProduct $bodyProduct): self
    {
        if ($this->bodyProducts->contains($bodyProduct)) {
            $this->bodyProducts->removeElement($bodyProduct);
            // set the owning side to null (unless already changed)
            if ($bodyProduct->getSubCategory() === $this) {
                $bodyProduct->setSubCategory(null);
            }
        }

        return $this;
    }
}
