import React, { Component } from 'react';
import axios from "axios"
import config from "./../config"

class Ariane extends Component {
    constructor(props) {
        super(props)
        this.state = {
            way: [],
            error: true,
        }
    }
    componentDidMount() {
        axios.get(`${config.api_host}/api/categories/`)
            .then((res) => {
                this.setState({ way: res.data["hydra:member"] })
                this.setState({ error: false })
            })
    }
    render() {
        return (
            <div>
                <h1>{this.state.way.name}</h1>
            </div>
        );
    }
}

export default Ariane;