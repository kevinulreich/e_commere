import React, { Component } from 'react'
import config from './../../config'
import axios from 'axios'
import "./../../css/AllProducts.sass"
import "./../../css/basket.sass"
import 'bootstrap/dist/css/bootstrap.min.css';

class ItemPanier extends Component {
	constructor(props) {
		super(props)
		this.state = {
			image:"",
			name: "",
			quantity: props.data.quantity,
			price: "",
			discount:"",
			stock: "",
			stock_id:"",
			productId: "",
			id_product: null
		}
	}
	
	componentDidMount(){
		axios.get(`${config.api_host}/api/products/${this.props.data.id_produit}`)
		.then(res =>{
			this.setState({
				id_product: res.data.id,
				price: res.data.price
			})

			axios.get(`${config.api_host}${res.data.bodyProduct}`)
			.then(resbp =>{
				this.setState({name: resbp.data.name})
			})

			axios.get(`${config.api_host}${res.data.stock}`)
			.then(resStock=>{
				this.setState({
					stock: resStock.data.quantity,
					stock_id: resStock.data["@id"],
					productId: resStock.data.product
				})
			})

			if(res.data.discounts.length > 0)
			{
				var axiosDiscounts = res.data.discounts.map(discounts=> {
					return axios.get(`${config.api_host}${discounts}`)
				})
				Promise.all(axiosDiscounts)
				.then(resDiscounts =>{
					resDiscounts.map(discount=>{
						var dateDebutPromotion = new Date(discount.data.startedAt)
						var dateFinPromotion = new Date(discount.data.endedAt)
						var dateActuelle = new Date()
						if(dateDebutPromotion < dateActuelle && dateActuelle < dateFinPromotion)
							this.setState({price: discount.data.newPrice})
						else
							this.setState({price: res.data.price})
						return false
					})
				})
			}
			if(res.data.images.length > 0)
			{
				var axiosImage = res.data.images.map(image=>{
					return axios.get(`${config.api_host}${image}`)
				})
				Promise.all(axiosImage)
				.then(resImages =>{
					var path = "";
					resImages.map(responses => {
						if(responses.data.main)
							path = responses.data.path
						return false
					})
					if(!path)
					{
						path = resImages[0].data.path
					}
					this.setState({image: path})
				})
			}
			else
			{
				this.setState({image: "https://via.placeholder.com/200"})
			}
		})	
	}
	handleChange(e){
		let new_value = e.target.value
		let newStock = this.state.stock
		if(this.state.quantity > new_value)
			newStock++
		else
			newStock--
		
		axios({
			method: "PUT",
			url: config.api_host + this.state.stock_id,
			data: {
				quantity: newStock,
				product: this.state.productId
			},
			headers: {
				"Content-Type": "application/json"
			},
			json: true
		})
		.then(res => {
			let panier = JSON.parse(localStorage.getItem("panier"))
			for(let i = 0; i < panier.length; i++)
				if(panier[i].id_produit === this.state.id_product)
					panier[i].quantity = parseInt(new_value, 10)
			localStorage.setItem("panier", JSON.stringify(panier))
			this.setState({
				stock: newStock, 
				quantity: new_value
			})
		})
	}
	handleClick()
	{
		let panier = JSON.parse(localStorage.getItem("panier"))
		for(let i = 0; i < panier.length; i++)
		if(panier[i].id_produit === this.state.id_product)
			panier.splice(i, 1)
		localStorage.setItem("panier", JSON.stringify(panier))
		let newStock = this.state.stock + this.state.quantity
		axios({
			method: "PUT",
			url: config.api_host + this.state.stock_id,
			data: {
				quantity: newStock,
				product: this.state.productId
			},
			headers: {
				"Content-Type": "application/json"
			},
			json: true
		})
		this.props.onClick(panier)
	}
	render()
	{
     	return(
			<div className="basket_item">
				<figure className="basket_figure">
					<img src={this.state.image} alt="product"/>
				</figure>
				<h1>{this.state.name}</h1>
				<div className="quantity">
				<p>Nombre d'articles commandés</p>
				<input type="number" value={this.state.quantity} min="1"  max={this.state.stock} onChange={(e)=>this.handleChange(e)}/>
				</div>
				<button type="button" onClick={() => this.handleClick()} className="basket_delete">
					<img src='https://img.icons8.com/ios/20/FB2B58/trash.png' alt="trash"/>
				</button>
            </div>
		)
 }
}

export default ItemPanier;