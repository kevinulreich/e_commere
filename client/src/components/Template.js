import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import ProductDetail from "./Product/ProductDetail"
import Home from "./Pages/Home"
import Error404 from "./Error/Error404"
import SearchBar from './Product/SearchBar'
import SelectCategory from './Product/SelectCategory'
import SearchResults from './Product/SearchResults'
import CategoryLoader from './Pages/CategoryLoader'
import Panier from './Pages/Panier'
import Register from './Auth/Register'
import FilArianne from "./Product/FilArianne"
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios"
import config from "./../config"
import Login from './Auth/Login';

class Template extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            modal_register: false,
            modal_login: false
        }
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }
    componentDidMount()
    {
        this.getCategories()
    }
    getCategories()
    {
        axios.get(`${config.api_host}/api/categories`)
        .then(res => {
            return res.data["hydra:member"].map(category => {
                let categoryObj = {
                    id: category.id,
                    name: category.name,
                    subcategories: category.subCategories
                }
                return categoryObj
            })
        })
        .then(res => {
            return res.map(category => {
                let subcatagoriesPromises = []
                category.subcategories.map(api => {
                    subcatagoriesPromises.push(axios.get(`${config.api_host}${api}`))
                    return true
                })
                return {
                    id: category.id,
                    name: category.name,
                    subcatagoriesPromises: subcatagoriesPromises
                }
            })
        })
        .then(allCategories => {
            allCategories.map(category => {
                let promises = []
                category.subcatagoriesPromises.map(promise => {
                    promises.push(promise)
                    return true
                })
               return Promise.all(promises)
                .then(res => {
                    let subcategoriesName = []
                    res.map(axiosRes => {
                        subcategoriesName.push({
                            id: axiosRes.data.id,
                            name: axiosRes.data.name
                        })
                        return true
                    })
                    let actualCategories = this.state.categories
                    actualCategories.push({
                        id: category.id,
                        name: category.name,
                        subCategories: subcategoriesName
                    })
                    this.setState({categories: actualCategories})
                })
            })
        })
    }
    searchByName(name, category)
    {
        let paramType = ""
        switch(category.type)
        {
            case "bodyProduct.subCategory.id":
                paramType = "subcategory"
                break;
            case "bodyProduct.subCategory.category.id":
                paramType = "category"
                break;
            default: 
                paramType = null
        }
        if(paramType)
            window.location = `${config.host}search/${name}?${paramType}=${category.id}`
        else
            window.location = `${config.host}search/${name}`
    }
    openModal(name) {
        switch(name)
        {
            case "connexion":
                document.getElementsByClassName("modal_connexion")[0].classList.toggle("modal_connexion_open")
                this.setState({modal_login: true})
                break;
            case "inscription":
                document.getElementsByClassName("modal_inscription")[0].classList.toggle("modal_inscription_open")
                this.setState({modal_register: true})
                break;
            default:
        }
    }
    closeModal(name) {
        switch(name)
        {
            case "connexion":
                document.getElementsByClassName("modal_connexion")[0].classList.toggle("modal_connexion_open")
                this.setState({modal_login: false})
                break;
            case "inscription":
                document.getElementsByClassName("modal_inscription")[0].classList.toggle("modal_inscription_open")
                this.setState({modal_register: false})
                break;
            default:
        }
    }
    logout()
    {
        localStorage.removeItem("user")
        localStorage.removeItem("secretKey")
        window.location = config.host
    }
    getMenu()
    {
        let log = <li onClick={this.logout} className="access">Déconnexion</li>
        if(!localStorage.getItem("user"))
            log = <>
                <li><p className="access" onClick={() => this.openModal("inscription")}>Inscription</p></li>
                <li><p className="access" onClick={() => this.openModal("connexion")}>Connexion</p></li>
            </>
        return (
            <ul className="nav">
                <li><a href={`${config.host}`}>AlKelipau.com</a></li> 
                <li>
                <SearchBar handleSubmit={(name, category) => this.searchByName(name, category)} categories={this.state.categories} />
                </li>
                {log}
                <li><a href={`${config.host}panier`}>Panier</a></li>
            </ul>
        )
    }
    render() {
        return (
            <div>
                <div className={ this.state.navClass }>
                    {this.getMenu()}
                </div>
                <div className="modal_inscription">
                    <Register closeModal={() => this.closeModal("inscription")} />
                </div>
                <div className="modal_connexion">
                    <Login closeModal={() => this.closeModal("connexion")} onLog={(usr, cb) => this.props.onLog(usr, cb) }/>
                </div>
                <div>
                    <div className="content">
                        <SelectCategory searchCategory={this.state.categories}/>
                        <div className="content_carousel">
                            <FilArianne url={this.props.location.pathname} />
                            <Router>
                                <Switch>
                                    <Route exact path="/">
                                        <Home />
                                    </Route>
                                    <Route path="/panier" component={Panier} />
                                    <Route path="/product/:id" render={(props) => <ProductDetail {...props} /> } />
                                    <Route path="/category/:id" render={(props) => <CategoryLoader {...props} /> } />
                                    <Route path="/subcategory/:id" render={(props) => <CategoryLoader {...props} /> } />
                                    <Route path="/search/:search" render={(props) => <SearchResults {...props} /> } />
                                    <Route path="/*">
                                        <Error404 />
                                    </Route>
                                </Switch>
                            </Router>
                        </div>        
                    </div>
                </div>
            </div>
        );
    }
}

export default Template;