import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Carousel } from 'react-bootstrap';
import config from "./../../config"

class Car extends Component {
    render() {
        return (
          <div className="carousel_parent">
            <Carousel>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={`${config.host}/images/slider_ecran.jpg`}
                  alt="First slide"
                />
              </Carousel.Item>

              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={`${config.host}/images/slider_ecran02.jpg`}
                  alt="Second slide"
                />
                
              </Carousel.Item>
              
              <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={`${config.host}/images/simpson.jpg`}
                    alt="Third slide"
                  />
              </Carousel.Item>
            </Carousel>
          </div>
        );
    }
}

export default Car;