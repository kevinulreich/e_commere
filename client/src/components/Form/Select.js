import React, { Component } from 'react'

export default class Input extends Component {
  render(){
    return(
        <div className="form_input">
          <label htmlFor={this.props.for}>{this.props.label}</label>
          <select name={this.props.name} id={this.props.for}  onChange={(e) => this.props.onChange(e)}>
              {this.props.options.map((opt) => {
                  return <option key={opt.value} value={opt.value}>{opt.content}</option>
              })}
          </select>
        </div>
    );
  }
}