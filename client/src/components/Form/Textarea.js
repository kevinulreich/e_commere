import React, { Component } from 'react'

export default class Input extends Component {
  render(){
    return(
        <div className="form_input">
          <label htmlFor={this.props.for}>{this.props.label}</label>
          <textarea name={this.props.name} id={this.props.for}  onChange={(e) => this.props.onChange(e)} value={this.props.value}></textarea>
        </div>
    );
  }
}
