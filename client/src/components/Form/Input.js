import React, { Component } from 'react'

export default class Input extends Component {
  render(){
    return(
        <div className="form_input">
          <label htmlFor={this.props.for}>{this.props.label}</label>
          <input 
            type={this.props.type} 
            name={this.props.name} 
            placeholder={this.props.placeholder} 
            id={this.props.for} 
            value={this.props.value}
            multiple
            onChange={(e) => this.props.onChange(e)}/>
        </div>
    );
  }
}
