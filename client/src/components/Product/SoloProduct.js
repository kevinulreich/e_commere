import React, { Component } from 'react';
import config from "./../../config";
import axios from 'axios';

class SoloProduct extends Component {
    constructor(props)
    {
        super(props)
        this.state = {
            image: null,
            body: {
                name: "",
                description: "",
                id: null
            }
        }
        this.getBody()        
    }
    componentDidMount() { 
        if (this.props.prod.images[0]) {
            this.props.prod.images.map((path, i) => {
               return this.getImage(path, i)
            }) 
        }
        else {
            this.setState({image: "https://via.placeholder.com/150"})
        }
    }
    getBody() {
        axios.get(`${config.api_host}${this.props.prod.bodyProduct}`)
        .then((res) => {
            this.setState({body: res.data})
        })
    }
    getImage(api, i)
    {
        axios.get(`${config.api_host}${api}`)
        .then((res) => {
            if (res.data.main || i === 0) {
                this.setState({image: res.data.path})
            }
        })
    }
    sizeText(string, n) {
        if(string.length > n) 
            return string.substring(-1, n) + " ..."
        return string
    }
    render() {
        return (
            <div className="all_products_product">
                <a href={`${config.host}product/${this.props.prod.id}`}>
                    <div className="fig--wrapper">
                        <figure className="all_products_image">
                            <img alt={`${this.state.body.name}`} className="all_images" src={this.state.image} />
                        </figure>
                    </div>
                    <div className="product_name">
                        <h4>{this.sizeText(this.state.body.name, 20)}</h4>
                        <p>{this.sizeText(this.state.body.description, 50)}</p>
                    </div>
                </a>
            </div>
        );
    }
}
export default SoloProduct;