import React, { Component } from 'react';
import config from "./../../config"

class DetailsColor extends Component {
    constructor(props) {
        super(props);
        this.color = React.createRef()
    }
    componentDidMount()
    {
        this.color.current.style.backgroundColor =  this.props.product.color
    }
    render() {
        return (
            <a href={`${config.host}product/${this.props.product.id}`}>
                <div className="detailsColor" ref={this.color}></div>
            </a>
        );
    }
}

export default DetailsColor;