import React, { Component } from 'react';
import "./../../css/ProductDetail.sass";
import axios from "axios";
import config from './../../config';

class AddBasket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: [],
            quantity: 1,
            stock: props.product.stock,
            discountPrice: props.discountPrice,
            price: props.product.price,
            priceClass: "",
            newPriceClass: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        this.displayPrice(this.state.price, this.state.discountPrice)
    }
    handleSubmit(e) { 
        e.preventDefault();
        axios({
            method: "PUT",
            url: `${config.api_host}/api/stocks/${this.props.product.stockId}`,
            data: {
                "quantity": this.state.stock - this.state.quantity,
                "product": this.props.product.productId
            },
            headers: {
                "Content-Type": "application/json"
            },
            json: true
        })
        .then((res) => {
            this.setState({ stock: res.data.quantity })
            this.props.update(res.data.quantity)
            var panier = localStorage.getItem("panier") ? JSON.parse(localStorage.getItem("panier")) : []
            let newQuantity = this.state.quantity
            for(let i = 0; i < panier.length; i++) {
                if(panier[i].id_produit === this.props.product.id) {
                    newQuantity = parseInt(panier[i].quantity, 10) + parseInt(this.state.quantity, 10)
                    panier.splice(i, 1)
                }
            }
            var item = {
                "id_produit": this.props.product.id,
                "quantity": newQuantity
            }
            panier.push(item)
            localStorage.setItem("panier", JSON.stringify(panier))
        })
    }
    displayPrice(a, b) {
        a = parseInt(a, 10)
        b = parseInt(b, 10)
        if(a === b) {
            this.setState({ 
                priceClass: "noDiscounts",
                newPriceClass: "noDisplayDiscount"
            })
        }
        else if(a !== b) {
            this.setState({
                priceClass: "yesDiscounts",
                newPriceClass: "displayDiscount"
            })
        }
    }
    
    render() {
        return (
            <div>
                <div className="add_product">
                    <div className={this.state.newPriceClass}>
                        <div>
                            <p className="normalPrice">{this.state.price} €</p>
                        </div>
                        <div>
                            <p className="product_detail_price">{this.state.discountPrice} €</p>
                        </div>
                    </div>
                    <div className={this.state.priceClass}>
                        <div>
                            <p className="product_detail_price">{this.state.price} €</p>
                        </div>
                    </div>
                    <form className="quantityForm" onSubmit={this.handleSubmit}>
                        <div className="qtity">
                            <label htmlFor="quantity">Quantité</label>
                            <input type="number" name="quantity" id="quantity" min="1" max={ this.state.stock } placeholder="1" onChange= {(e) => { this.setState({ quantity: e.target.value }) }} />
                        </div>
                        <div className="btn_vert">
                            <div className="bouton">
                        </div>
                            <button type="submit" className="add_panier"> <span className="credits"></span> Ajouter au panier</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default AddBasket;