import React, { Component } from 'react';
import config from "./../../config"
import axios from "axios"
import "./../../css/searchbar.sass"
import 'bootstrap/dist/css/bootstrap.min.css';
import ToggleCategories from "./ToggleCategories"

class SearchBar extends Component {
    constructor(props)
    {
        super(props)
        this.state = {
            items: [],
            focused: null,
            categoriesSelected: {
                type: "default",
                id: "default"
            }
        }
    }
    handleSubmit(e)
    {
        e.preventDefault()
        let input_value = document.querySelector(".searchbar_input").value
        if(input_value)
            this.props.handleSubmit(input_value, this.state.categoriesSelected)
    }
    handleChange(e)
    {
        this.clearList()
        if( e.target.value.length > 0)
            this.updateList(e.target.value)
    }
    updateList(search)
    {
        let searchParams = this.getSearchParams(search)
        axios.get(`${config.api_host}/api/products${searchParams}`)
        .then((res) => {
            return res.data["hydra:member"].map(product => {
                return {
                    infos: product,
                    bodyProductPromise: axios.get(`${config.api_host}${product.bodyProduct}`),
                    imagesPromises: product.images.map(image => {
                        return axios.get(`${config.api_host}${image}`)
                    })
                }
            })
        })
        .then(products => {
            let bodyProductPromise = products.map(product => {
                return product.bodyProductPromise
            })
            let imagesPromises = products.map(product => {
                return product.imagesPromises
            })
            let infoProduct = products.map(product => {
                return product.infos
            })
            Promise.all(bodyProductPromise)
            .then((...res) => {
                return res[0].map((data, i) => {
                    return {
                        id: infoProduct[i].id,
                        name: data.data.name,
                        color: infoProduct[i].color,
                        imagesPromises: imagesPromises[i]
                    }
                })
            })
            .then(data => {
                return data.map(product => {
                    return Promise.all(product.imagesPromises)
                    .then(results => {
                        let path = ""
                        results.map(res => {
                            if(res.data.main)
                                path = res.data.path
                            return true
                        })
                        if(!path)
                            if(results.length > 0)
                                path = results[0].data.path
                            else
                                path = "https://via.placeholder.com/150"

                        return path
                    })
                    .then(path => {
                        let items = this.state.items
                        items.push({
                            id: product.id,
                            name: product.name,
                            color: product.color,
                            image: path
                        })
                        this.setState({items: items})
                    })
                })
            })
        })
    }
    getSearchParams(search)
    {
        let categorySearch = ``
        let categorySelected = this.state.categoriesSelected
        if(categorySelected && categorySelected.type !== "default")
            categorySearch = `&${categorySelected.type}=${categorySelected.id}`
        return `?bodyProduct.name=${search}${categorySearch}`
    }
    clearList()
    {
        this.setState({items: []})
    }
    showList()
    {
        return this.state.items.map((item, i) => {
            if( i < 6)
                return (
                <div 
                    key={`${item.id}`}  
                    tabIndex="0" 
                    className="searchbar_item"
                    onKeyDown={(e) => {
                        return e.key === "Enter" ? this.validItem(item.id) : null}} 
                    onClick={(e) => this.validItem(item.id)}
                >
                    <figure className="searchbar_item_image">
                        <img src={item.image} alt="illustration" />
                    </figure>
                    {item.name} - {item.color}
                </div>
            )
            return true
        })
    }
    validItem(id)
    {
        window.location = `${config.host}product/${id}`
    }
    moveInList(e)
    {
        if(e.key === "ArrowDown")
            this.giveFocus(0)
    }
    giveFocus(elemToGiveFocus)
    {
        let nbrOfChilds = this.state.items.length
        if(nbrOfChilds > elemToGiveFocus)
        {
            document.querySelectorAll(".searchbar_item")[elemToGiveFocus].focus()
            this.setState({focused: elemToGiveFocus})
        }
    }
    changeFocus(e)
    {
        let focused = this.state.focused
        if( e.key === "ArrowDown")
        {
            if((focused + 1) < this.state.items.length)
            {
                document.querySelectorAll(".searchbar_item")[focused + 1].focus()
                this.setState({focused: focused + 1})
            }
            else
            {
                document.querySelectorAll(".searchbar_item")[0].focus()
                this.setState({focused: 0})
            }
        }
        if( e.key === "ArrowUp")
        {
            if((focused - 1) >= 0)
            {
                document.querySelectorAll(".searchbar_item")[focused - 1].focus()
                this.setState({focused: focused - 1})
            }
            else
            {
                document.querySelector(".searchbar_input").focus()
                this.setState({focused: null})
            }
        }
    }
    isActive()
    {
        if(this.state.items.length > 0)
            return "searchbar_item_container_active"
    }
    closeCategories()
    {
        let all_categories = document.getElementsByClassName("togglecategories_all")[0]
        all_categories.classList.remove("togglecategories_all_active")

        document.getElementsByClassName("togglecategories")[0].classList.remove("togglecategories_open")
    }
    render() {
        return (
            <form autoComplete="off" className="searchbar" onSubmit={(e) => this.handleSubmit(e)}>
                <ToggleCategories 
                    categories={this.props.categories} 
                    onChange={(type, id) => this.setState({categoriesSelected: {
                        type: type,
                        id: id
                    }})} 
                />
                <input type="text" className="searchbar_input" placeholder="Rechercher produit" onChange={(e) => this.handleChange(e)} onKeyDown={(e) => this.moveInList(e)} onFocus={() => this.closeCategories()} />
                <div className={this.isActive()} onKeyUp={(e) => this.changeFocus(e)}>
                    {this.showList()}
                </div>
            </form>
        );
    }
}

export default SearchBar;