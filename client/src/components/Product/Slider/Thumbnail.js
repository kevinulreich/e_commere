import React, { Component } from 'react';

class Thumbnail extends Component {
    render() {
        return (
            <figure className="slider_thumbnail" onClick={() => this.props.onClick()}>
                <img className="img_thumbnails" src={this.props.src} alt="thumbnail" />
            </figure>
        );
    }
}

export default Thumbnail;