import React, { Component } from 'react';
import Thumbnail from './Thumbnail'
import axios from "axios"
import config from "./../../../config"

class Slider extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            current: 0,
            paths: ["https://via.placeholder.com/150"]
        }
        this.getImages(props.paths)
    }
    getImages(apis)
    {
        apis.map((path) => {
            return axios.get(`${config.api_host}${path}`)
            .then((res) => {
                let allPaths = this.state.paths
                allPaths.push(res.data.path)
                this.setState({paths: allPaths})
            })
        })
    }
    changeImage(pos)
    {
        this.setState({current: pos})
    }
    thumbnail()
    {
        return this.state.paths.map((path, i) => {return <Thumbnail key={path} src={path} onClick={() => this.changeImage(i)} />})
    }
    render() {
        if( this.state.paths)
        {
            return (
                <div className="slider">
                    <figure>
                        <img className="img_principale"
                            src={this.state.paths[this.state.current]}
                            alt="illustration"
                        />
                    </figure>
                    <div className="slider_all_images">
                        {this.thumbnail()}
                    </div>
                </div>
            );
        }
        else
            return ''
    }
}

export default Slider;