
import React, { Component } from 'react';
import axios from "axios";
import config from "./../../config"
import Select from '../Form/Select';

class SearchProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categories:[],
            error: true
        }
    }
    handleChange(e)
    {
        e.preventDefault()
        this.props.handleChange(e.target.value)
    }
    componentDidMount() {
        axios.get(`${config.api_host}/api/categories`)
        .then((res) => {
            this.setState({ categories: res.data["hydra:member"] })
            this.setState({ error: false })
        })
    }
    render() {
        if( !this.state.error)
        {
            let dataCat = [];
            this.state.categories.map(category => {
               dataCat.push({
                    key: category.id,
                    value: category.id,
                    content: category.name
               })           
            })         
            return (
                <Select
                    name="category"
                    for="inp_category"
                    label="Catégorie : "
                    options= {dataCat}
                    onChange={(e) => this.handleChange(e)}
                />
            )
       }
        else
            return "We've encountered an error"          
    }
}

export default SearchProduct;