import React, { Component } from 'react';
import "./../../css/ProductIndispo.sass"


class ProductIndispo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: []
        }
    }

    render() {
        return (
            <div>
                <div className="add_product">

                    <h4 className="product_detail_price">{this.props.price} €</h4>
                    <div className="qtity">
                    <p className="txt_indispo">Ce produit n'est plus en stock</p>
                    </div>
                    <div className="btn_vert">
                        <div className="bouton_ind">
                        </div>
                        <button class="indispo"> Indisponible</button>
                    </div>

                </div>

            </div>
        );
    }
}

export default ProductIndispo;