import React, { Component } from 'react';

class ToggleCategories extends Component {
    constructor(props)
    {
        super(props)
        this.state = {
            categorySelected: "Toutes les catégories"
        }
    }
    toggleCategories()
    {
        let all_categories = document.getElementsByClassName("togglecategories_all")[0]
        all_categories.classList.toggle("togglecategories_all_active")

        document.getElementsByClassName("togglecategories")[0].classList.toggle("togglecategories_open")
    }
    selectCategory(type, id, name)
    {
        this.setState({categorySelected: name})
        this.props.onChange(type, id)
    }
    getCategories()
    {
        return this.props.categories.map((category) => {
            let categoryDetails = [
                <h5 
                    key={`c${category.id}`}
                    className="togglecategories_option_category"
                    onClick={() => this.selectCategory("bodyProduct.subCategory.category.id", category.id, category.name)}
                    onKeyDown={(e) => {
                        return e.key === "Enter" ? e.target.click() : null
                    }}
                    tabIndex="0"
                >{category.name}</h5>
            ]
            category.subCategories.map(subCategory => {
                categoryDetails.push(
                    <h6 
                        key={`s${subCategory.id}`}
                        className="togglecategories_option_subcategory"
                        onClick={() => this.selectCategory("bodyProduct.subCategory.id", subCategory.id, subCategory.name)}
                        onKeyDown={(e) => {
                            return e.key === "Enter" ? e.target.click() : null
                        }}
                    tabIndex="0"
                    >{subCategory.name}</h6>)
                return true
            })
            return (
                <div key={`o${category.id}`} className="togglecategories_option">
                    {categoryDetails}
                </div>
            )
        })
    }
    render() {
        return (
            <div 
                className="togglecategories" 
                tabIndex="0" 
                onClick={() => this.toggleCategories()}
            >
                <p className="togglecategories_selected">{this.state.categorySelected}</p>
                <div className="togglecategories_all">
                    <h6 
                        className="togglecategories_option_category"
                        onClick={() => this.selectCategory("default", "default", "Toutes les catégories")}
                        tabIndex="0"
                    >Toutes les catégories</h6>
                    {this.getCategories()}
                </div>
            </div>
        );
    }
}

export default ToggleCategories;