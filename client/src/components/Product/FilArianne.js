import React, {
    Component
} from 'react';
import axios from 'axios'
import config from "./../../config"

class FilArianne extends Component {

    constructor(props) {
        super(props);

        this.state ={
            FilAriane : [
                <a key={"home"} href="/">Accueil</a>  
            ]
        }
    }
    componentDidMount(){
        var str = this.props.url
        var arr = str.split("/")
        var type = arr[1]
        var id = arr[2]

        switch (type) {
            case 'product':
                var route_api = "/api/products/" + id
                axios.get(`${config.api_host}${route_api}`)
                .then(data=>{
                    axios.get(`${config.api_host}${data.data.bodyProduct}`)
                    .then(product=>{
                        var product_name = product.data.name
                        axios.get(`${config.api_host}${product.data.subCategory}`)
                        .then(subCategory =>{
                            var subCategory_name = subCategory.data.name
                            axios.get(`${config.api_host}${subCategory.data.category}`)
                            .then(category =>{
                                var category_name = category.data.name
                                var fildariane = this.state.FilAriane
                                fildariane.push(
                                    " > ", 
                                    <a key="category" href={`${config.host}category/${category.data.id}`}>{category_name}</a>,
                                    " > ",
                                    <a  key="subctegory" href={`${config.host}subcategory/${subCategory.data.id}`}>{subCategory_name}</a>,
                                    " > ",
                                    <span key="product">{product_name}</span>
                                )
                                this.setState({FilAriane:fildariane})
                            })
                        })
                    })
                })

                break;
            case 'category':
                route_api = "/api/categories/" + id
                axios.get(`${config.api_host}${route_api}`)
                    .then(data=>{
                        var fildariane = this.state.FilAriane
                        fildariane.push(" > ",<span key="category">{data.data.name}</span>)
                        this.setState({FilAriane:fildariane})
                    })

                break;
            case 'subcategory':
                route_api = "/api/sub_categories/" + id
                axios.get(`${config.api_host}${route_api}`)
                    .then(data=>{
                        axios.get(`${config.api_host}${data.data.category}`)
                        .then(category =>{
                            var fildariane = this.state.FilAriane
                            fildariane.push(
                                " > ",
                                <a key="category" href={`${config.host}category/${category.data.id}`}>{category.data.name}</a>,
                                " > ",
                                <span key="product">{data.data.name}</span>
                            )
                            this.setState({FilAriane:fildariane})
                        })
                    })
                break;
            default:
                break;
        }
        return route_api
    }
    render() {
        return (
            <div className="filArianne">
                {this.state.FilAriane}
            </div> 
        );
    }
}

export default FilArianne;