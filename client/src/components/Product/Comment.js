import React, { Component } from 'react';
import "./../../css/ProductDetail.sass";

class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: true,
            comments: props.comm.comments
        }
    }
    dateFormat(date)
    {
        let comDate = new Date(date)
        let visualDate = `${this.twoDigits(comDate.getDate() - 1)}/${this.twoDigits(comDate.getMonth())}/${comDate.getFullYear()} à ${this.twoDigits(comDate.getHours())}:${this.twoDigits(comDate.getMinutes())}`
        return visualDate
    }
    twoDigits(num)
    {
        return ("0" + (num + 1)).slice(-2)
    }
    render() {
        const comments = this.state.comments.map((comment, i) => {
            return (
                <div key={i} className="comment">
                    <div className="pseudo_note">
                        <p className="pseudo_comment">{comment.pseudo}</p>
                        <p className="note_comment">{comment.note}/5</p>
                    </div>
                    <p className="date_comment">Posté le: {this.dateFormat(comment.date)}</p>
                    <pre className="comment">{comment.content}</pre>

                </div>)
        })
        return (<div >
            <h4 className="title_comm">Les avis clients:</h4>
            <div className="all_comment">

                {comments}
            </div>

        </div>
        );
    }
}

export default Comment;