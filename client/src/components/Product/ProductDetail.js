import React, { Component } from 'react';
import Slider from "./Slider/Slider"
import Error404 from "../Error/Error404"
import axios from "axios"
import config from "./../../config"
import "./../../css/ProductDetail.sass"
import AddBasket from "./AddBasket"
import ProductIndispo from './ProductIndispo';
import Comment from './Comment'
import PostComm from './PostComm';
import DetailsColor from './DetailsColor';
import SimpleCrypto from "simple-crypto-js"

class ProductDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: false,
            isLoaded: false,
            product: [],
            stock: 0
        }
        this.updateStock = this.updateStock.bind(this)
    }
    getUserId()
    {
        if(!localStorage.getItem("user"))
            return null
        var secretKey = localStorage.getItem("secretKey")
        var crypto =  new SimpleCrypto(secretKey)
        return JSON.parse(crypto.decrypt(localStorage.getItem("user"))).id
    }
    componentDidMount() {
        axios({
            method: "POST",
            url: `${config.api_host}/api/visitors`,
            data: {
                visitedAt: new Date(),
                product: `/api/products/${this.props.match.params.id}`,
                user: `/api/users/${this.getUserId()}`
            },
            headers: {
                "Content-Type": "application/json"
            },
            json: true
        })
        axios.get(`${config.api_host}/api/products/${this.props.match.params.id}`)
        .then((res) => {
                        
            return {
                id: res.data.id,
                bodyProduct: axios.get(`${config.api_host}${res.data.bodyProduct}`),
                depth: res.data.depth,
                height: res.data.height,
                images: res.data.images,
                price: res.data.price,
                stock: axios.get(`${config.api_host}${res.data.stock}`),
                width: res.data.width,
                color: res.data.color,
                weight: res.data.weight,
                unavailable: res.data.unavailable,
                discounts: res.data.discounts,
                comments: res.data.comments
            }
        })
        .then((details) => {
            Promise.all([details.stock])
            .then(resStock => {
                Promise.all([details.bodyProduct])
                .then(resBodyProduct => {
                    let discountPromise = details.discounts.map(api => {
                        return axios.get(config.api_host + api)
                    })
                    Promise.all(discountPromise)
                    .then(resDiscount => {
                        let other_productsPromises = resBodyProduct[0].data.products.map(apiProduct => {
                            return axios.get(config.api_host + apiProduct)
                        })
                        Promise.all(other_productsPromises)
                        .then(resOtherProducts => {
                            let otherProducts = resOtherProducts.map(otherdetailsProduct => {
                                return {
                                    id: otherdetailsProduct.data.id,
                                    color: otherdetailsProduct.data.color,
                                    weight: otherdetailsProduct.data.weight
                                }
                            })
                            let commentsPromises = details.comments.map(apiComment => {
                                return axios.get(config.api_host + apiComment)
                            })
                            Promise.all(commentsPromises)
                            .then(resComments => {
                                return resComments.map(resOneComment => {
                                    return {
                                        date: resOneComment.data.createdAt,
                                        content: resOneComment.data.content,
                                        pseudo: resOneComment.data.pseudo,
                                        note: resOneComment.data.note,
                                    }
                                })
                            })
                            .then(comments => {
                                comments.sort((a, b) => {
                                    
                                    return new Date(a.date) < new Date(b.date) ? 1 : -1
                                })
                                this.setState({
                                    product: {
                                        id: details.id,
                                        name: resBodyProduct[0].data.name,
                                        description: resBodyProduct[0].data.description,
                                        depth: details.depth,
                                        height: details.height,
                                        images: details.images,
                                        price: details.price,
                                        discountPrice: this.getRealPrice(resDiscount, details.price),
                                        stock: resStock[0].data.quantity,
                                        stockId: resStock[0].data.id,
                                        color:details.color,
                                        width: details.width,
                                        weight: details.weight,
                                        unavailable: details.unavailable,
                                        otherProducts: otherProducts,
                                        comments: comments
                                    },
                                    isLoaded: true,
                                })
                            })
                        })
                    })
                })
            })
        
        })
        .catch(err => {
            this.setState({error: true})
        })
    }
    updateStock(newStock) {
        this.setState({stock: newStock })
    }
    getRealPrice(resDiscount, defaultPrice)
    {
        let discounts = resDiscount.map(resultDiscount => {
            let actualDate = new Date();
            let startDate = new Date(resultDiscount.data.startedAt)
            let endDate = new Date(resultDiscount.data.endedAt)
            let current = (actualDate >= startDate && actualDate <= endDate) ? true : false
            return {
                current: current,
                newPrice: resultDiscount.data.newPrice
            }
        })
        for(let i = 0; i < discounts.length; i++)
            if(discounts[i].current)
                return discounts[i].newPrice
        return defaultPrice
    }

    render() {
        var basket = <AddBasket product={this.state.product} discountPrice={this.state.product.discountPrice} update={this.updateStock}/>;
        if (this.state.product.stock < 1 || this.state.product.unavailable === true)
            basket = <ProductIndispo price={this.state.product.price} />

        if (this.state.isLoaded) {
            let detailsColor = this.state.product.otherProducts.map(otherProduct => {
                return <DetailsColor key={otherProduct.id} product={otherProduct} />
            })
            return (
                <section className="product_detail_container">
                    <header className="product_detail_container_header">
                        <h2 className="product_detail_title">{this.state.product.name}</h2>
                        <hr />
                    </header>
                    <div className="product_detail_description">
                        <Slider paths={this.state.product.images} />
                        <div className="product_detail_infos">
                            <p className="product_detail_text">{this.state.product.description}</p>
                            <div className="detailsColor_container">
                                {detailsColor}
                            </div>
                            <h4 className="product_detail_stock">Stock : {this.state.product.stock}</h4>
                            <div className="product_detail_dimension">
                                <h4 className="product_detail_poids">Poids : {this.state.product.weight}g</h4>
                                <h4 className="product_detail_width">Largeur : {this.state.product.width}cm</h4>
                                <h4 className="product_detail_height">Hauteur : {this.state.product.height}cm</h4>
                                <h4 className="product_detail_depth">Profondeur: {this.state.product.depth}cm</h4>
                            </div>
                            {basket}
                            <Comment comm={this.state.product} />
                        </div>
                    </div>
                    <PostComm id_product={this.state.product}/>
                </section>
            );
        }
        else if(this.state.error) {
            return <Error404 />
        }
        else
        {
            return (
                <div className="loader">
                    <svg height="100" width="250">
                        <circle cx="50" cy="50%" r="0"  fill="#FFFFFF">
                            <animate attributeType="XML" attributeName="r" from="0" to="25" dur="1s" repeatCount="indefinite"begin="0s"/>
                        </circle>
                        <circle cx="125" cy="50%" r="0" fill="#FFFFFF">
                            <animate attributeType="XML" attributeName="r" from="0" to="25" dur="1s" repeatCount="indefinite" begin="0.05s"/>
                        </circle>
                        <circle cx="200" cy="50%" r="0" fill="#FFFFFF">
                            <animate attributeType="XML" attributeName="r" from="0" to="25" dur="1s" repeatCount="indefinite"begin="0.1s"/>
                        </circle>
                    </svg>
                </div>
          )
        }
    }
}

export default ProductDetail;