import React, { Component } from 'react';
import axios from "axios"
import config from "./../../config"
import Products from './Products';

class SearchResults extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            products: []
        }
    }
    componentDidMount()
    {
        this.updateList()
    }
    updateList()
    {
        let searchParams = this.getSearchParams()
        axios.get(`${config.api_host}/api/products${searchParams}`)
        .then((res) => {
            this.setState({
                products:res.data["hydra:member"],
                isLoaded: true})
        })
    }
    getSearchParams()
    {
        let category = []
        let search = this.props.match.params.search
        for(let p of new URLSearchParams(this.props.location.search))
            category.push(p)
        let categorySearch = ``
        if(category.length > 0)
        {
            if(category[0][0] === "category")
                categorySearch = `&bodyProduct.subCategory.category.id=${category[0][1]}`
            else
                categorySearch = `&bodyProduct.subCategory.id=${category[0][1]}`
        }
        return `?bodyProduct.name=${search}${categorySearch}`
    }
    render() {
        if(!this.state.isLoaded)
            return <React.Fragment>Chargement</React.Fragment>
        return <Products 
            allProducts={this.state.products} 
            title={`- ${this.props.match.params.search} : `} 
            message={this.state.products.length < 1 ? `Nous n'avons rien trouvé pour votre recherche...` : ""}
        />
    }
}

export default SearchResults;