import React, { Component } from 'react';
import SoloProduct from "./SoloProduct";
import "./../../css/AllProducts.sass"

class Products extends Component {
    render() {
        let allProducts = this.props.allProducts.map((product) => { return <SoloProduct key={product.id} prod={product}/> })
        let message = this.props.message
        return (
            <div className="productsSection">
                <div className ="productsSection--title">
                    <h2>{this.props.title}</h2>
                    <hr/>
                </div>
                <div className="all_products">
                    {allProducts}
                    <div className="message">
                        {message}
                    </div>
                </div>
            </div>
        );
    }
}

export default Products;