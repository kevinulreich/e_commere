import React, { Component } from 'react';
import axios from 'axios'
import "./../../css/ProductDetail.sass";
import config from "./../../config"

class PostComm extends Component {
    constructor(props) {
        super(props);
        this.state = { pseudo: '', note: 1, comm: '', product:props.id_product.id }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        axios({
            method: "POST",
            url: `${config.api_host}/api/comments`,
            data: {
                pseudo: this.state.pseudo,
                note: parseInt(this.state.note, 10),
                content: this.state.comm,
                createdAt: new Date(),
                product: `/api/products/${this.state.product}`
            },
            headers: {
                "Content-Type": "application/json"
            },
            json: true
        })
        .then(res => {
            window.location = `${config.host}product/${this.state.product}`
        })
    }
    render() {
        return (
            <div>
                <form className="form_comm" onSubmit={(e)=>this.handleSubmit(e)} action="">
                    <input name='pseudo' className="pseudo_comm" type="text" value={this.state.pseudo}
                        onChange={(e) => { this.setState({ pseudo: e.target.value }) }} placeholder="Entre votre nom" />
                    <hr />
                    <textarea type='text' name='comm' className="comm_pdt" placeholder="veuillez laissez votre commentaire sur ce produit" value={this.state.comm}
                        onChange={(e) => { this.setState({ comm: e.target.value }) }}></textarea>
                    <label htmlFor="note-select">Choisissez une note:</label>

                    <select name="note" id="note-select" value={ this.state.note} onChange={(e) => { this.setState({ note: e.target.value }) }}>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <button className="btn_comm" type="submit" value="Valider">Soummetre</button>
                </form>
            </div>
        );
    }
}

export default PostComm;