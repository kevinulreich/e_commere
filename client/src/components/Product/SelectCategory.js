import React, { Component } from 'react';
import "./../../css/selectCategory.sass";
import config from "./../../config";

class SelectCategory extends Component {
    render() {
        const allCategories = this.props.searchCategory.map((categories, i) => 
        {
            const allSub = categories.subCategories.map((sub, k) => 
            {
                return <li key={k}><a href={`${config.host}subcategory/${sub.id}`}>{sub.name}</a></li>;
            });
              return (
                        <div key={i}>
                            <details open>
                                <summary><a href={`${config.host}category/${categories.id}`}>{categories.name}</a></summary>
                                {allSub}
                            </details>
                        </div>
                    );
        });
        return <div className="category">
                    <div className="category_title">
                        <p>CATEGORIES</p>
                    </div>
                    <div>
                        {allCategories}
                    </div>
                </div>
    }
}

export default SelectCategory;

