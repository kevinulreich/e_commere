import React, { Component } from 'react';
import Products from './../Product/Products'
import Car from "./../Carousels/Car"
import config from './../../config'
import axios from 'axios'
import "./../../css/AllProducts.sass"


class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            titleProdSection: "Tous les produits"
        }
    }
    componentDidMount() {
        axios.get(`${config.api_host}/api/products`)
        .then((res) => {
            
            this.setState({ products: this.sortByVisitor(res.data["hydra:member"]) })
        })
    }
    sortByVisitor(prodArray) {
        prodArray.sort(function(a, b) {
            return a.visitors.length < b.visitors.length ? 1 : -1   
        })
        return prodArray
    }
    render() {
        return (
            <React.Fragment> 
                <Car/>
                <Products allProducts={this.state.products} title={this.state.titleProdSection} />
            </React.Fragment>)
    }
}

export default Home;