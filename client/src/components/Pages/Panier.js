import React, { Component } from 'react';
import ItemPanier from './../Panier/ItemPanier'
import "./../../css/AllProducts.sass"
import 'bootstrap/dist/css/bootstrap.min.css';

class Panier extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }
    componentDidMount() {
        if(JSON.parse(localStorage.getItem("panier"))) {
            this.setState({items:JSON.parse(localStorage.getItem("panier"))})
        }
    }
    setItem(newPanier)
    {
        this.setState({items: newPanier})
    }
    render(){
        const allItems = this.state.items.map((item, i) => {
            return <ItemPanier key={i} data={item} onClick={(itms => this.setItem(itms))}/>
        })
        if(allItems.length > 0)
            return(
                <div>
                    <div className="productsSection--title">
                        <h2>Panier</h2>
                        <hr/>
                    </div>
                    {allItems}
                    <button className="basket_buy" disabled>Acheter</button>
                </div>
            )
        else
            return <p>Il n'y a aucun produit dans le panier</p>
   }
}

export default Panier;