import React, { Component } from 'react';
import Products from './../Product/Products'
import config from './../../config'
import axios from 'axios'
import "./../../css/AllProducts.sass"
import 'bootstrap/dist/css/bootstrap.min.css';

class BySubcategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            titleProdSection: "",
            message: "",
            exist: false,
            pathCategory: "",
            pathProduct: ""
        }
    }

    componentDidMount() { 
        const paths = this.props.match.path;
        var result = paths.split("/");
        var pathCategory = "";
        var pathProduct = "";
        if (result[1] === "subcategory")
        {
            pathCategory = "/api/sub_categories/" 
            pathProduct = "/api/products?bodyProduct.subCategory.id="

        }
        else
        {
            pathCategory = "/api/categories/";
            pathProduct = "/api/products?bodyProduct.subCategory.category.id=";
        }
        this.setState({pathCategory: pathCategory });
        this.setState({pathProduct: pathProduct});

        axios.get(`${config.api_host}${pathCategory}${this.props.match.params.id}`)
        .then((res) => {
            if(res) {
                this.setState({
                    titleProdSection: res.data.name,
                    exist: true
                })
                this.getCategoryName()
            }
        })
        .catch(err => {    
            if(Number.isInteger(parseInt(this.props.match.params.id, 10))) {
                this.setState({ message: "Cette catégorie n'existe pas !"})
            }
            else {
                this.setState({message: `${this.props.match.params.id} : cette catégorie n'existe pas !`})
            }         
        })
    }
    getCategoryName() {
        if (Number.isInteger(parseInt(this.props.match.params.id, 10)) && this.state.exist) {
            axios.get(`${config.api_host}${this.state.pathProduct}${this.props.match.params.id}`)
            .then((res) => {
                if(res.data["hydra:member"].length > 0) {
                    this.setState({ products: res.data["hydra:member"] })
                }
                else {
                    this.setState({ message: "Il n'y a aucun produit dans cette catégorie !"})
                }
                this.setState({titleProdSection:this.state.titleProdSection + this.plurialize(res.data["hydra:member"].length)})
            })
        }
    }
    plurialize(n) {
        if(n > 1)
            return ' : ' + n + ' produits trouvés'
        return ' : ' + n + ' produit touvé'
    }
    render() {
        return <Products 
        allProducts={this.state.products} 
        title={this.state.titleProdSection} 
        message={this.state.message}
        />
    }
}

export default BySubcategory;