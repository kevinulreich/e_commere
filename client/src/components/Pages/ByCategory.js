import React, { Component } from 'react';
import Products from './../Product/Products'
import config from './../../config'
import axios from 'axios'
import "./../../css/AllProducts.sass"
import 'bootstrap/dist/css/bootstrap.min.css';

class ByCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            titleProdSection: "",
            message: "",
            exist: false
        }
    }
    componentDidMount() {
        axios.get(`${config.api_host}/api/categories/${this.props.match.params.id}`)
        .then((res) => {
            if(res) {
                this.setState({
                    titleProdSection: res.data.name,
                    exist: true
                })
                this.getAllProducts()
            }
        })
        .catch(err => {    
            if(Number.isInteger(parseInt(this.props.match.params.id, 10))) {
                this.setState({ message: "Cette catégorie n'existe pas !"})
            }
            else {
                this.setState({message: `${this.props.match.params.id} : cette catégorie n'existe pas !`})
            }         
        })
    }
    getAllProducts() {
        if (Number.isInteger(parseInt(this.props.match.params.id, 10)) && this.state.exist) {
            axios.get(`${config.api_host}/api/products?bodyProduct.subCategory.category.id=${this.props.match.params.id}`)
            .then((res) => {
                if(res.data["hydra:member"].length > 0) {
                    this.setState({ products: res.data["hydra:member"] })
                }
                else {
                    this.setState({ message: "Il n'y a aucun produit dans cette catégorie !"})
                }
                this.setState({titleProdSection:this.state.titleProdSection + this.plurialize(res.data["hydra:member"].length)})
            })
        }  
    }
    plurialize(n) {
        let string
        if(n > 1)
            return string = ' : ' + n + ' produits trouvés'
        return string = ' : ' + n + ' produit touvé'
    }
    render() {
        return <Products 
            allProducts={this.state.products} 
            title={this.state.titleProdSection}
            message={this.state.message}
        />
    }
}

export default ByCategory;