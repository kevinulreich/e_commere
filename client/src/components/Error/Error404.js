import React, { Component } from 'react';
import "./../../css/error.sass"

class Error404 extends Component {
    render() {
        return (
            <div className="error">
                <h1 className="error_code">404</h1>
                <h2 className="error_details">Page non trouvée :(</h2>
            </div>
        );
    }
}

export default Error404;