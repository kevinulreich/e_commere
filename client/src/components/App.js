import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Error404 from "./Error/Error404"
import Admin from "./Admin"
import Template from "./Template"
import 'bootstrap/dist/css/bootstrap.min.css';
import SimpleCrypto from "simple-crypto-js";

class App extends Component {
    log(usr, callback)
    {
        var secretKey = SimpleCrypto.generateRandom();
        localStorage.setItem("secretKey", secretKey)
        var crypto =  new SimpleCrypto(secretKey)
        let hash = crypto.encrypt(JSON.stringify(usr))
        localStorage.setItem("user", hash)
        callback()
    }
    isAdmin()
    {
        var secretKey = localStorage.getItem("secretKey")
        var crypto =  new SimpleCrypto(secretKey)
        return JSON.parse(crypto.decrypt(localStorage.getItem("user"))).roles.includes("ROLE_ADMIN")
    }
    render() {
        let reserved = ''
        if( localStorage.getItem("user"))
            if(this.isAdmin())
                reserved = <Route path="/admin" component = {Admin}/>
        return (
            <div>
                <Router>
                    <Switch>
                        {reserved}
                        <Route exact path="/" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)} /> } />
                        <Route path="/panier" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)} /> } />
                        <Route path="/product/:id" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)} /> } />
                        <Route path="/category/:id" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)}/> } />
                        <Route path="/subcategory/:id" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)}/> } />
                        <Route path="/search/:search" render={(props) => <Template {...props} onLog={(usr, cb) => this.log(usr, cb)}/> } />
                        <Route path="/*">
                            <Error404 />
                        </Route>
                    </Switch>
                </Router>
                
            </div>
        );
    }
}

export default App;