import React, { Component } from 'react';
import crypto from "crypto";
import axios from "axios";
import config from './../../config';
import Input from "./../Form/Input";
import "./../../css/Form.sass"

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            name: '', 
            email: '', 
            password: '',
            hash: '', 
            message: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelButton = this.cancelButton.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault();
        axios({
            method: "POST",
            url: `${config.api_host}/api/users`,
            data: {
                name: this.state.name,
                email: this.state.email,
                password: this.state.hash,
                createdAt: new Date()
            },
            headers: {
                "Content-Type": "application/json"
            },
            json: true
        })
        .then((res) => {
            this.setState({ newUser: res.data, message: 'Vous êtes maintenant enregistré sur notre site.' })
            this.props.closeModal()
        })
        .catch((err) => {
            this.setState({ newUser: "Error with the API", message: "Une erreur s'est produite." })
        })
        
        this.setState({ name: '', email: '', password: '', hash: '', createdAt: '' , message: ''})
    }

    hashPassword(e){
        let pass = e.target.value;
        const hash = crypto.createHash("sha512");
        hash.update(pass);
        this.setState({
            password: pass,
            hash: hash.digest("hex")
        });
    }
    cancelButton() {
        this.setState({ name: '', email: '', password: '', hash: '', createdAt: '' , message: ''})
        this.props.closeModal() 
    }
    openModal() {
        this.setState({ 
            registerClass: "registerContent",
            navClass: "nav__hidden"
         })
    }
    closeModal() {
        this.setState({ 
            registerClass: "registerContent__hidden",
            navClass: "nav__display"
         })
    }
    render() {
        return (
            <div className="form--wrapper">
                <div className="register--form">
                    <p className="cancel__button" onClick={this.cancelButton}>x</p>       
                    <h2>Inscription</h2>
                    <form onSubmit={this.handleSubmit}>
                        <Input 
                            type= 'text'
                            name= 'register_name'
                            placeholder= 'Pseudo'
                            for= 'register_name'
                            value= {this.state.name}
                            onChange= {(e) => { this.setState({ name: e.target.value }) }}
                            />
                        <Input 
                            type= 'email'
                            name= 'register_email'
                            placeholder= 'email@abc.fr'
                            for= 'register_email'
                            value= {this.state.email}
                            onChange= {(e) => { this.setState({ email: e.target.value }) }}
                            />
                        <Input 
                            type= 'password'
                            name= 'register_password'
                            placeholder= 'Mot de passe'
                            for= 'register_password'
                            value= {this.state.password}
                            onChange= {e => this.hashPassword(e)}
                            />
                        <div className="button--container">
                            <div className="button--wrapper">
                                <input className="button" type="submit" value="Valider" />
                            </div>
                        </div>
                    </form>
                    <p className="registerMessage">{ this.state.message }</p>
                </div>
            </div>
        );
    }
}

export default Register;
