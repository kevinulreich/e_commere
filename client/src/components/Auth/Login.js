import React, { Component } from 'react';
import crypto from "crypto";
import axios from "axios";
import config from './../../config';
import Input from "../Form/Input";
import "./../../css/Form.sass"

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '', 
            password: '',
            hash: '',
            message: '' 
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelButton = this.cancelButton.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault();
        axios.get(`${config.api_host}/api/users?email=${this.state.email}`)
        .then((res) => {
            let data = res.data["hydra:member"][0]
            if(!data || data.password !== this.state.hash)
            {
                this.setState({message: "Vos identifiants sont incorrects"})
                return false
            }
            this.props.onLog({
                id: data.id,
                pseudo: data.username,
                email: data.email,
                roles: data.roles
            }, _=> {
                window.location = config.host
            })
        })
    }

    hashPassword(e){
        let pass = e.target.value;
        const hash = crypto.createHash("sha512");
        hash.update(pass);
        this.setState({
            password: pass,
            hash: hash.digest("hex")
        });
    }

    cancelButton() {
        this.setState({ name: '', email: '', password: '', hash: '', createdAt: '' , message: ''})
        this.props.closeModal() 
    }

    render() {
        return (
            <div className="form--wrapper">
                <div className="register--form">
                    <p className="cancel__button" onClick={this.cancelButton}>x</p>       
                    <h2>Connexion</h2>
                    <form onSubmit={this.handleSubmit}>
                        <Input 
                            type= 'email'
                            name= 'login_email'
                            placeholder= 'email@abc.fr'
                            for= 'login_email'
                            value= {this.state.email}
                            onChange= {(e) => { this.setState({ email: e.target.value }) }}
                            />
                        <Input 
                            type= 'password'
                            name= 'login_password'
                            placeholder= 'Mot de passe'
                            for= 'login_password'
                            value= {this.state.password}
                            onChange= {e => this.hashPassword(e)}
                            />
                        <div className="button--container">
                            <div className="button--wrapper">
                                <input className="button" type="submit" value="Valider" />
                            </div>
                        </div>
                    </form>
                    <p className="registerMessage">{ this.state.message }</p>
                </div>
            </div>
        );
    }
}

export default Login;
